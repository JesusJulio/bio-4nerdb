lazy val root = (project in file("."))
  .settings(
    name := "4nerdb",
    libraryDependencies ++= Dependencies.test
  )

lazy val commonSettings = Seq(
  scalaVersion := "2.13.1",
  version := "0.1.0-SNAPSHOT",
  organization := "com.example",
  organizationName := "example",
  testFrameworks += new TestFramework("minitest.runner.Framework")
)

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
