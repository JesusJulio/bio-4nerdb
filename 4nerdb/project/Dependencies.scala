import sbt._

object Dependencies {
  object Versions {
    lazy val minitest = "2.7.0"
  }

  lazy val test = Seq(
    "io.monix" %% "minitest" % Versions.minitest % s"it,$Test")
}
